package altimetrik;

import java.util.*;

/**
 * Created by akash on 25-03-2016.
 */
public class OZs {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int[] outputs = new int[t];
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int nO = in.nextInt();
            int nZ = in.nextInt();
            List<String> strings = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                strings.add(in.next());
            }
            Collections.sort(strings, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    int rs = Integer.valueOf(o1.length()).compareTo(o2.length());
                    if(rs != 0){
                        return rs;
                    } else {
                        if(nO < nZ){

                        }

                    }
                    return rs;
                }
            });
            outputs[i] = numberOf(strings, nO, nZ);
        }
        for (int i = 0; i < t; i++) {
            System.out.println(outputs[i]);
        }
    }

    private static int numberOf(List<String> strings, int nO, int nZ) {
        int retval = 0;
        for (String string : strings) {
            int countO = countOfO(string);
            int countZ = countOfZ(string);
            if (countO <= nO && countZ <= nZ) {
                retval++;
                nO -= countO;
                nZ -= countZ;
            }
        }
        return retval;
    }

    private static int countOfO(String string) {
        return string.length() - string.replaceAll("O", "").length();
    }

    private static int countOfZ(String string) {
        return string.length() - string.replaceAll("Z", "").length();
    }

}
