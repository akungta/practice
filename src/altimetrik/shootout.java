package altimetrik;

import java.util.Scanner;

/**
 * Created by akash on 25-03-2016.
 */
public class shootout {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        String[] outputs = new String[t];
        for (int i = 0; i < t; i++) {
            int ax = in.nextInt();
            int ay = in.nextInt();
            int bx = in.nextInt();
            int by = in.nextInt();
            int cx = in.nextInt();
            int cy = in.nextInt();
            int dx = in.nextInt();
            int dy = in.nextInt();
            outputs[i] = bulletPath(ax, ay, bx, by, cx, cy, dx, dy);
        }
        for (int i = 0; i < t; i++) {
            System.out.println(outputs[i]);
        }
    }

    private static String bulletPath(int ax, int ay, int bx, int by, int cx, int cy, int dx, int dy) {

        double acm = (double) (cy - ay) / (cx - ax);
        double bdm = (double) (dy - by) / (dx - bx);

        double abm = (double) (by - ay) / (bx - ax);
        double adm = (double) (dy - ay) / (dx - ax);

        double bam = (double) (ay - by) / (ax - bx);
        double bcm = (double) (cy - by) / (cx - bx);

        if (acm != abm && acm != adm && bdm != bam && bdm != bcm
                && linesIntersect(ax, ay, cx, cy, bx, by, dx, dy)) {
            return "No";
        }

        return "Yes";
    }

    public static boolean linesIntersect(double x1, double y1,
                                         double x2, double y2,
                                         double x3, double y3,
                                         double x4, double y4) {
        return ((relativeCCW(x1, y1, x2, y2, x3, y3) *
                relativeCCW(x1, y1, x2, y2, x4, y4) <= 0)
                && (relativeCCW(x3, y3, x4, y4, x1, y1) *
                relativeCCW(x3, y3, x4, y4, x2, y2) <= 0));
    }

    public static int relativeCCW(double x1, double y1,
                                  double x2, double y2,
                                  double px, double py) {
        x2 -= x1;
        y2 -= y1;
        px -= x1;
        py -= y1;
        double ccw = px * y2 - py * x2;
        if (ccw == 0.0) {
            ccw = px * x2 + py * y2;
            if (ccw > 0.0) {
                px -= x2;
                py -= y2;
                ccw = px * x2 + py * y2;
                if (ccw < 0.0) {
                    ccw = 0.0;
                }
            }
        }
        return (ccw < 0.0) ? -1 : ((ccw > 0.0) ? 1 : 0);
    }

}
