import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 21-02-2016.
 */
public class Permute {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> input = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            input.add(i, in.nextInt());
        }
        List<List<Integer>> outputs = new ArrayList<>();
        permutate(input, 0, outputs);
        System.out.println(outputs);
    }


    public static void permutate(List<Integer> list, int index, List<List<Integer>> outputs) {
        if (index == list.size()) {
            outputs.add(list);
        }
        for (int i = index; i < list.size(); i++) {
            List<Integer> permutation = new ArrayList<>(list);
            permutation.set(index, list.get(i));
            permutation.set(i, list.get(index));
            permutate(permutation, index + 1, outputs);
        }
    }

}
