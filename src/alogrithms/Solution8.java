package alogrithms;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by akash on 07-02-2016.
 */
public class Solution8 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int R = in.nextInt();
            int C = in.nextInt();
            String G[] = new String[R];
            for(int G_i=0; G_i < R; G_i++){
                G[G_i] = in.next();
            }
            int r = in.nextInt();
            int c = in.nextInt();
            String P[] = new String[r];
            for(int P_i=0; P_i < r; P_i++){
                P[P_i] = in.next();
            }
            System.out.println(isPresent(R,C,G,r,c,P));
        }
    }

    private static String isPresent(int R, int C, String[] g, int r, int c, String[] p) {
        for(int i = 0; i < R - r + 1; i++){
            int j = 0;
            if(g[i].contains(p[j])){
                int[] indexsOf = indexsOf(g[i],p[j]);
                for(int indexOf : indexsOf) {
                    int k = i + 1;
                    boolean flag = true;
                    for (j = 1; j < r; j++) {
                        if (g[k].substring(indexOf, indexOf + c).equals(p[j])) {
                            k++;
                            continue;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        return "YES";
                    }
                }
            }
        }
        return "NO";
    }

    private static int[] indexsOf(String s, String subStr) {
        int[] retval = new int[s.length()];
        int i=0;
        int indx=0;
        while(s.contains(subStr)){
            retval[i] = s.indexOf(subStr) + indx;
            indx = retval[i] + 1;
            s = s.substring(s.indexOf(subStr)+1, s.length());
            i++;
        }
        return Arrays.copyOf(retval, i);
    }

}
