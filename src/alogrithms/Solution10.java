package alogrithms;

import java.util.Scanner;

/**
 * Created by akash on 07-02-2016.
 */
public class Solution10 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        int n = in.nextInt();
        int r = in.nextInt();
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = in.nextInt();
            }
        }
        print(rotate(m, n, r, a));
    }

    private static void print(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            StringBuilder sb = new StringBuilder();
            boolean f = false;
            for (int j = 0; j < a[0].length; j++) {
                if (f) {
                    sb.append(" ");
                } else {
                    f = true;
                }
                sb.append(a[i][j]);
            }
            System.out.println(sb.toString());
        }
    }

    private static int[][] rotate(int m, int n, int r, int[][] a) {
        int cycles = Math.min(m, n) / 2;
        int[] rings = new int[cycles];
        int x = m, y = n;
        for (int c = 0; c < cycles; c++) {
            rings[c] = x * y;
            x = x - 2;
            y = y - 2;
        }
        for (int c = 0; c < cycles - 1; c++) {
            rings[c] = rings[c] - rings[c + 1];
        }
        int x1 = 0, y1 = 0, x2 = m - 1, y2 = n - 1;
        for (int c = 0; c < cycles; c++) {
            a = rotateCylce(r % rings[c], x1, y1, x2, y2, a, m, n);
            x1++;
            x2--;
            y1++;
            y2--;
        }
        return a;
    }

    private static int[][] rotateCylce(int rotationNumber, int x1, int y1, int x2, int y2, int[][] a, int m, int n) {
        if (rotationNumber == 0) {
            return a;
        }

        int current = a[x1][y1];
        int next = 0;
        for (int i = x1; i < x2; i++) {
            next = a[i + 1][y1];
            a[i + 1][y1] = current;
            current = next;
        }

        for (int i = y1; i < y2; i++) {
            next = a[x2][i + 1];
            a[x2][i + 1] = current;
            current = next;
        }

        for (int i = x2; i > x1; i--) {
            next = a[i - 1][y2];
            a[i - 1][y2] = current;
            current = next;
        }

        for (int i = y2; i > y1; i--) {
            next = a[x1][i - 1];
            a[x1][i - 1] = current;
            current = next;
        }
        return rotateCylce(--rotationNumber, x1, y1, x2, y2, a, m, n);
    }

}
