package booking.com;

import java.util.Scanner;

/**
 * Created by akash on 10-04-2016.
 */
public class Polygons {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int square = 0;
        int rect = 0;
        int poly = 0;
        while (in.hasNextLine()) {
            if(!in.hasNextInt()){
                break;
            }
            int a = in.nextInt();
            int b = in.nextInt();
            int c = in.nextInt();
            int d = in.nextInt();
            if (a > 0 && b > 0 && c > 0 && d > 0) {
                if (a == b && b == c && c == d && d == a) {
                    square++;
                } else if (a == c && b == d) {
                    rect++;
                } else {
                    poly++;
                }
            } else {
                poly++;
            }
        }
        System.out.println(square + " " + rect + " " + poly);

    }


}
