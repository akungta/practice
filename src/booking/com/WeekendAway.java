package booking.com;

import java.util.*;

/**
 * Created by akash on 20-02-2016.
 */
public class WeekendAway {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int[] outputs = new int[t];
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            Map<Integer, List<Integer>> directions = new HashMap<>();
            Map<Map.Entry<Integer, Integer>, Integer> distances = new HashMap<>();
            for (int j = 0; j < m; j++) {
                int a = in.nextInt();
                int b = in.nextInt();
                int d = in.nextInt();
                putInDrections(directions, a, b);
                putInDrections(directions, b, a);
                distances.put(getKey(a, b), d);
            }
            outputs[i] = getMin(n, m, directions, distances);
        }
        for (int output : outputs) {
            System.out.println(output);
        }

    }

    private static void putInDrections(Map<Integer, List<Integer>> directions, int a, int b) {
        if (directions.containsKey(a)) {
            directions.get(a).add(b);
        } else {
            List<Integer> l = new ArrayList<>();
            l.add(b);
            directions.put(a, l);
        }
    }

    private static AbstractMap.SimpleEntry<Integer, Integer> getKey(int a, int b) {
        return new HashMap.SimpleEntry<>(Integer.min(a, b), Integer.max(a, b));
    }

    private static int getMin(int n, int m, Map<Integer, List<Integer>> directions, Map<Map.Entry<Integer, Integer>, Integer> distances) {

        int min = Integer.MAX_VALUE;
        for (int iN = 1; iN <= n; iN++) {
            int d = 0;
            List<Integer> nexts = directions.get(iN);
            for (int j = 0; j < nexts.size(); j++) {
                for (int k = j + 1; k < nexts.size(); k++) {
                    int jN = nexts.get(j);
                    int kN = nexts.get(k);
                    if ((distances.get(getKey(iN, jN)) + distances.get(getKey(iN, kN))) < min) {
                        min = distances.get(getKey(iN, jN)) + distances.get(getKey(iN, kN));
                    }
                }
            }
        }
        return min;
    }


    private static int getMin2(int n, int m, Map<Integer, List<Integer>> directions, Map<Map.Entry<Integer, Integer>, Integer> distances) {

        int min = Integer.MAX_VALUE;
        for (Map.Entry<Map.Entry<Integer, Integer>, Integer> entry : distances.entrySet()) {
            int a = entry.getKey().getValue();
            int b = entry.getKey().getKey();
            int d = entry.getValue();
            if (d < min) {
                for (Integer nextNode : directions.get(a)) {
                    if (nextNode != b) {
                        int nextD = distances.get(getKey(a, nextNode));
                        if (d + nextD < min) {
                            min = d + nextD;
                        }
                    }
                }
            }
            d = entry.getValue();
            if (d < min) {
                for (Integer nextNode : directions.get(b)) {
                    if (nextNode != a) {
                        int nextD = distances.get(getKey(b, nextNode));
                        if (d + nextD < min) {
                            min = d + nextD;
                        }
                    }
                }
            }
        }

        return min;
    }


}
