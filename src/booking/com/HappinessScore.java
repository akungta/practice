package booking.com;

import java.util.*;

/**
 * Created by akash on 21-02-2016.
 */
public class HappinessScore {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> inputs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            inputs.add(in.nextInt());
        }
        Set<Integer> numbers = new HashSet<>();
        for (int i = 1; i <= n; i++) {
            numbers.addAll(getCombinations(i, inputs));
        }
        int count = 0;
        for (int number : numbers) {
            if (isPrime(number)) {
                count++;
            }
        }

        System.out.println(count);

    }

    private static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        if (n <= 3) {
            return true;
        }
        if (n % 2 == 0 || n % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i <= n; i += 6) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }

    public static Set<Integer> getCombinations(int elementCount, List<Integer> inputs) {
        int N = inputs.size();

        if (elementCount < 1 || elementCount > N) {
            throw new IndexOutOfBoundsException(
                    "Can only generate permutations for a count between 0 to " + N);
        }

        Set<Integer> retval = new HashSet<>();

        if (elementCount == N) {
            int sum = 0;
            for (int input : inputs) {
                sum += input;
            }
            retval.add(sum);
        } else if (elementCount == 1) {
            for (int i = 0; i < N; i++) {
                retval.add(inputs.get(i));
            }
        } else {
            for (int i = 0; i <= N - elementCount; i++) {
                List<Integer> subList = inputs.subList(i + 1, inputs.size());

                Set<Integer> pSums = getCombinations(elementCount - 1, subList);
                for (int pSum : pSums) {
                    retval.add(pSum + inputs.get(i));
                }
            }
        }

        return retval;
    }

}
