package booking.com;

import java.util.*;

/**
 * Created by akash on 21-02-2016.
 */
public class VisitingAttractions {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int id = 0;
        Point h = new Point(id, in.nextInt(), in.nextInt());
        int n = in.nextInt();
        List<Edge> edges = new ArrayList<>();
        List<Point> locations = new ArrayList<>();
        id = 1;
        for (int i = 0; i < n; i++) {
            Point l = new Point(id++, in.nextInt(), in.nextInt());
            //hotel to location
            edges.add(new Edge(h, l, distance(h, l) / 5));
            for (Point pl : locations) {
                double d = distance(pl, l) / 5;
                //location to location
                edges.add(new Edge(pl, l, d));
                edges.add(new Edge(l, pl, d));
            }
            locations.add(l);
        }
        int s = in.nextInt();
        List<Point> metros = new ArrayList<>();
        id = 101;
        for (int i = 0; i < s; i++) {
            Point sp = new Point(id++, in.nextInt(), in.nextInt());
            //hotel to metro
            edges.add(new Edge(h, sp, (distance(h, sp) / 5)));
            for (Point pl : locations) {
                double d = (distance(pl, sp) / 5);
                //location to metro, metro to location
                edges.add(new Edge(pl, sp, d));
                edges.add(new Edge(sp, pl, d));
            }
            metros.add(i, sp);
        }
        int l = in.nextInt();
        for (int i = 0; i < l; i++) {
            Point s1 = metros.get(in.nextInt() - 1);
            Point s2 = metros.get(in.nextInt() - 1);
            double d = distance(s1, s2) / 25;
            edges.add(new Edge(s1, s2, d));
            edges.add(new Edge(s2, s1, d));
        }

        List<Point> nodes = new ArrayList<>();
        nodes.add(h);
        nodes.addAll(locations);
        nodes.addAll(metros);

        List<List<Point>> orderOfLocations = new ArrayList<>();
        permutate(locations, 0, orderOfLocations);

        DijkstraAlgorithm algo = new DijkstraAlgorithm(nodes, edges);

        List<Point> traveledLocations = new ArrayList<>();
        for (List<Point> orderOfLocation : orderOfLocations) {

            List<Point> currentTraveledLocations = new ArrayList<>();
            double maxTime = 0;
            Point location = h;
            for (Point nextLocation : orderOfLocation) {
                algo.execute(location);
                double w = algo.getWeight(nextLocation);
                if (maxTime + w > 12) {
                    break;
                }
                maxTime = maxTime + w + 1;
                location = nextLocation;
                currentTraveledLocations.add(location);
            }
            if (currentTraveledLocations.size() > traveledLocations.size()) {
                traveledLocations.clear();
                traveledLocations.addAll(currentTraveledLocations);
            }
            if (locations.size() == traveledLocations.size()) {
                break;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (Point p : traveledLocations) {
            sb.append(p.id).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);

        System.out.println(traveledLocations.size());
        System.out.println(sb.toString());

    }

    public static <E> void permutate(List<E> list, int index, List<List<E>> outputs) {
        if (index == list.size()) {
            outputs.add(list);
        }
        for (int i = index; i < list.size(); i++) {
            List<E> permutation = new ArrayList<>(list);
            permutation.set(index, list.get(i));
            permutation.set(i, list.get(index));
            permutate(permutation, index + 1, outputs);
        }
    }


    public static class DijkstraAlgorithm {

        private final List<Point> nodes;
        private final List<Edge> edges;
        private Set<Point> settledNodes;
        private Set<Point> unSettledNodes;
        private Map<Point, Point> predecessors;
        private Map<Point, Double> weights;

        public DijkstraAlgorithm(List<Point> nodes, List<Edge> edges) {
            this.nodes = nodes;
            this.edges = edges;
        }

        public void execute(Point source) {
            settledNodes = new HashSet<Point>();
            unSettledNodes = new HashSet<Point>();
            weights = new HashMap<Point, Double>();
            predecessors = new HashMap<Point, Point>();
            weights.put(source, (double) 0);
            unSettledNodes.add(source);
            while (unSettledNodes.size() > 0) {
                Point node = getMinimum(unSettledNodes);
                settledNodes.add(node);
                unSettledNodes.remove(node);
                findMinimalDistances(node);
            }
        }

        private void findMinimalDistances(Point node) {
            List<Point> adjacentNodes = getNeighbors(node);
            for (Point target : adjacentNodes) {
                if (getShortestDistance(target) > getShortestDistance(node)
                        + getDistance(node, target)) {
                    weights.put(target, getShortestDistance(node)
                            + getDistance(node, target));
                    predecessors.put(target, node);
                    unSettledNodes.add(target);
                }
            }

        }

        private double getDistance(Point node, Point target) {
            for (Edge edge : edges) {
                if (edge.p1.equals(node)
                        && edge.p2.equals(target)) {
                    return edge.d;
                }
            }
            throw new RuntimeException("Should not happen");
        }

        private List<Point> getNeighbors(Point node) {
            List<Point> neighbors = new ArrayList<Point>();
            for (Edge edge : edges) {
                if (edge.p1.equals(node)
                        && !isSettled(edge.p2)) {
                    neighbors.add(edge.p2);
                }
            }
            return neighbors;
        }

        private Point getMinimum(Set<Point> Pointes) {
            Point minimum = null;
            for (Point Point : Pointes) {
                if (minimum == null) {
                    minimum = Point;
                } else {
                    if (getShortestDistance(Point) < getShortestDistance(minimum)) {
                        minimum = Point;
                    }
                }
            }
            return minimum;
        }

        private boolean isSettled(Point Point) {
            return settledNodes.contains(Point);
        }

        private double getShortestDistance(Point destination) {
            Double d = weights.get(destination);
            if (d == null) {
                return Integer.MAX_VALUE;
            } else {
                return d;
            }
        }

        public double getWeight(Point p) {
            return weights.get(p);
        }

    }


    static double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    static class Edge {
        final Point p1;
        final Point p2;
        final double d;

        public Edge(Point p1, Point p2, double d) {
            this.p1 = p1;
            this.p2 = p2;
            this.d = d;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge edge = (Edge) o;

            if (!p1.equals(edge.p1)) return false;
            return p2.equals(edge.p2);

        }

        @Override
        public int hashCode() {
            int result = p1.hashCode();
            result = 31 * result + p2.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "E{" + p1 +
                    "," + p2 +
                    "," + d +
                    '}';
        }
    }


    static class Point {
        final int id;
        final int x;
        final int y;

        public Point(int id, int y, int x) {
            this.id = id;
            this.y = y;
            this.x = x;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            return id == point.id;

        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public String toString() {
            return "P{" + id + "," + x + "," + y + "}";
        }
    }

}
