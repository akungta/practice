package booking.com;

import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by akash on 10-04-2016.
 */
public class SumOfTwoNumbers {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int sum = in.nextInt();
        int m = in.nextInt();
        boolean present = false;
        HashSet<Integer> diff = new HashSet<>();
        for (int i = 0; i < m; i++) {
            int n = in.nextInt();
            if (diff.contains(n)) {
                present = true;
                break;
            } else {
                diff.add(sum - n);
            }
        }
        System.out.println(present ? 1 : 0);


    }

}
