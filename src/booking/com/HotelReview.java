package booking.com;

import java.util.*;

/**
 * Created by akash on 10-04-2016.
 */
public class HotelReview {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        HashSet<String> reviewWords = new HashSet<>();
        for (String word : cleanAndSplit(in.nextLine())) {
            reviewWords.add(word);
        }

        int r = Integer.parseInt(in.nextLine());

        Map<Integer, Integer> hotel = new HashMap<>();

        for (int i = 0; i < r; i++) {
            int id = Integer.parseInt(in.nextLine());

            if (!hotel.containsKey(id)) {
                hotel.put(id, 0);
            }

            for (String s : cleanAndSplit(in.nextLine())) {
                if (reviewWords.contains(s)) {
                    hotel.put(id, hotel.get(id) + 1);
                }
            }
        }

        List<Map.Entry<Integer, Integer>> list =
                new LinkedList<>(hotel.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : list) {
            sb.append(entry.getKey()).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);

        System.out.println(sb.toString());
    }

    static String[] cleanAndSplit(String string) {
        return string.toLowerCase().replace(".", "").replace(",", "").split("\\s+");
    }

}
