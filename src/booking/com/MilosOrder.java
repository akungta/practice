package booking.com;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 21-02-2016.
 */
public class MilosOrder {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> inputs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            inputs.add(i, in.nextInt());
        }
        System.out.println(isMilosDiary(inputs) ? "YES" : "NO");
    }

    private static boolean isMilosDiary(List<Integer> inputs) {
        LinkedList<Integer> stack = new LinkedList<>();
        for (int input : inputs) {
            if (stack.isEmpty()) {
                stack.push(input);
            } else if (input == stack.peek()) {
                return false;
            } else if (input > stack.peek()) {
                stack.push(input);
            } else {
                LinkedList<Integer> tempStack = new LinkedList<>();
                while (!stack.isEmpty() && input <= stack.peek()) {
                    tempStack.push(stack.pop());
                }
                if (tempStack.peek() == input) {
                    return false;
                }
                if (tempStack.peek() - 1 == input) {
                    stack.push(input);
                } else {
                    return false;
                }
                while (!tempStack.isEmpty()) {
                    stack.push(tempStack.pop());
                }
            }
        }
        return true;
    }

}
