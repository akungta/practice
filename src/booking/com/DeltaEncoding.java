package booking.com;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 10-04-2016.
 */
public class DeltaEncoding {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String numbers = in.nextLine();
        List<Integer> input = new ArrayList<>();
        for (String number : numbers.split("\\s+")) {
            input.add(Integer.parseInt(number));
        }
        StringBuilder output = new StringBuilder();
        int x = input.get(0);
        output.append(x).append(" ");
        for (int i = 1; i < input.size(); i++) {
            x = input.get(i) - input.get(i - 1);
            if (-127 <= x && x <= 127) {
                output.append(x).append(" ");
            } else {
                output.append(-128).append(" ");
                output.append(x).append(" ");
            }
        }
        output.deleteCharAt(output.length()-1);
        System.out.println(output.toString());
    }
}
