import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 21-02-2016.
 */
public class Combination {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> inputs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            inputs.add(in.nextInt());
        }
        System.out.println(getCombinationsList(2, inputs));

    }

    public static List<List<Integer>> getCombinationsList(int elementCount, List<Integer> inputs) {
        int N = inputs.size();

        if (elementCount < 0 || elementCount > N) {
            throw new IndexOutOfBoundsException(
                    "Can only generate permutations for a count between 0 to " + N);
        }

        List<List<Integer>> retval = new ArrayList<>();

        if (elementCount == N) {
            retval.add(new ArrayList<>(inputs));
        } else if (elementCount == 1) {
            for(int i = 0 ; i < N ; i++){
                List<Integer> l = new ArrayList<>();
                l.add(inputs.get(i));
                retval.add(l);
            }
        } else {
            for (int i = 0; i <= N - elementCount; i++) {
                List<Integer> subList = inputs.subList(i + 1, inputs.size());

                List<List<Integer>> cList = getCombinationsList(elementCount - 1, subList);
                for (List<Integer> s : cList) {
                    s.add(inputs.get(i));
                    retval.add(s);
                }
            }
        }

        return retval;
    }

}
