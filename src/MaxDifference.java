/**
 * Created by akash on 23-03-2016.
 */
public class MaxDifference {

    public static void main(String[] args) {

    }

    static int maxDiff(int[] arr) {
        int maxDiff = arr[1] - arr[0];
        int minElement = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] - minElement > maxDiff) {
                maxDiff = arr[i] - minElement;
            }
            if (arr[i] < minElement) {
                minElement = arr[i];
            }
        }
        return maxDiff;
    }

}
