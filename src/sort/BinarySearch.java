package sort;

import java.util.Scanner;

/**
 * Created by akash on 08-02-2016.
 */
public class BinarySearch {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int val = in.nextInt();
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
        }
        System.out.println(indexOfIn(val, ar));

    }

    private static int indexOfIn(int val, int[] ar) {

        int start = 0;
        int end = ar.length -1;
        int m = (start + end) / 2;
        while (ar[m] != val) {

            if (ar[m] > val) {
                end = m - 1;
            } else if (ar[m] < val) {
                start = m + 1;
            }

            m = (start + end) / 2;
        }
        return m;
    }

}
