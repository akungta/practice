package blueoptima;

import java.util.Scanner;

/**
 * Created by akash on 01-04-2016.
 */
public class Puzzle {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        String[] outputs = new String[t];

        int[][] finalGrid = new int[4][4];
        int val = 1;
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                finalGrid[j][k] = val++;
            }
        }

        finalGrid[3][3] = 0;

        int x = 0, y = 0;
        for (int i = 0; i < t; i++) {
            int[][] grid = new int[4][4];
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    grid[j][k] = in.nextInt();
                    if (grid[j][k] == 0) {
                        x = j;
                        y = k;
                    }
                }
            }
            outputs[i] = canBeSolved(x, y, grid, grid, finalGrid);
        }

    }

    private static String canBeSolved(int x, int y, int[][] grid, int[][] initialGrid, int[][] finalGrid) {

        if (isSolved(grid, finalGrid)) {
            return "YES";
        }

        // go north
        if (x >= 1) {
            int[][] newGrid = get(grid);
            int temp = newGrid[x - 1][y];
            newGrid[x - 1][y] = newGrid[x][y];
            newGrid[x][y] = temp;
            canBeSolved(x - 1, y, newGrid, initialGrid, finalGrid);
        }
        // go south
        if (x <= 2) {
            int[][] newGrid = get(grid);
            int temp = newGrid[x + 1][y];
            newGrid[x + 1][y] = newGrid[x][y];
            newGrid[x][y] = temp;
            canBeSolved(x + 1, y, newGrid, initialGrid, finalGrid);
        }
        // go west
        if (y >= 1) {
            int[][] newGrid = get(grid);
            int temp = newGrid[x][y - 1];
            newGrid[x][y - 1] = newGrid[x][y];
            newGrid[x][y] = temp;
            canBeSolved(x, y - 1, newGrid, initialGrid, finalGrid);
        }
        // go east
        if (y <= 2) {
            int[][] newGrid = get(grid);
            int temp = newGrid[x][y + 1];
            newGrid[x][y + 1] = newGrid[x][y];
            newGrid[x][y] = temp;
            canBeSolved(x, y + 1, newGrid, initialGrid, finalGrid);
        }

        return "NO";
    }


    private static int[][] get(int[][] grid) {
        int[][] retval = new int[4][4];
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                retval[j][k] = grid[j][k];
            }
        }
        return retval;
    }

    private static boolean isSolved(int[][] grid, int[][] finalGrid) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                if (grid[j][k] != finalGrid[j][k]) {
                    return false;
                }
            }
        }
        return true;
    }


}
