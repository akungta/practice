package honeywell;

import java.util.Scanner;

/**
 * Created by akash on 01-05-2016.
 */
public class OzPainting {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        long[] outputs = new long[t];
        for (int i = 0; i < t; i++) {
            outputs[i] = numberOfPaintings(in.nextLong(), in.nextLong(), in.nextLong());
        }
        for (long output : outputs) {
            System.out.println(output);
        }

    }


    private static long numberOfPaintings(long w, long r, long g) {
        long retval = 0;

        long min = Math.min(Math.min(w, r), g);

        retval += min;

        w -= min;
        r -= min;
        g -= min;

        retval += w / 3;
        retval += r / 3;
        retval += g / 3;

        return retval;
    }

}
