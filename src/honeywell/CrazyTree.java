package honeywell;

import java.util.*;

/**
 * Created by akash on 01-05-2016.
 */
public class CrazyTree {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int l = in.nextInt();

        int q = in.nextInt();

        int[] n = new int[q];
        int[] x = new int[q];
        int[] y = new int[q];

        int leastL = l;
        for (int i = 0; i < q; i++) {
            n[i] = in.nextInt();
            x[i] = in.nextInt();
            y[i] = in.nextInt();

            if (n[i] < leastL) {
                leastL = n[i];
            }
        }

        Map<Integer, List<Integer>> tree = formTree(l, leastL);
        //System.out.println(tree);

        long[] outputs = new long[q];
        for (int i = 0; i < q; i++) {
            outputs[i] = value(n[i], x[i], y[i], tree);
        }
        for (long output : outputs) {
            System.out.println(output);
        }
    }

    private static Map<Integer, List<Integer>> formTree(int l, int least) {

        Map<Integer, List<Integer>> retval = new HashMap<>();
        for (int i = l; i >= least; i--) {
            List<Integer> list = new ArrayList<>();
            if (i == l) {
                for (long j = 1; j <= Math.pow(2, i - 1); j++) {
                    list.add((int) (j % 1299709));
                }
            } else {
                List<Integer> prev = retval.get(i + 1);
                for (int j = 1; j <= Math.pow(2, i - 1); j++) {
                    Integer multiply = prev.get((2 * j) - 2) * (prev.get((2 * j) - 1));
                    list.add(multiply % 1299709);
                }
            }
            retval.put(i, list);
        }
        return retval;
    }

    private static long value(int n, int x, int y, Map<Integer, List<Integer>> tree) {

        List<Integer> list = tree.get(n);
        Integer sum = 0;
        for (int i = x - 1; i <= y - 1; i++) {
            sum = (sum + list.get(i))%1299709;
        }

        return sum%1299709;
    }

}
