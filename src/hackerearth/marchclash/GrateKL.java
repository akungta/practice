package hackerearth.marchclash;

import java.util.*;

/**
 * Created by akash on 26-03-2016.
 */
public class GrateKL {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        List<Integer> cupidsOfNode = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cupidsOfNode.add(i, in.nextInt());
        }
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < n - 1; i++) {
            int v1 = in.nextInt();
            int v2 = in.nextInt();
            if (graph.containsKey(v1)) {
                graph.get(v1).add(v2);
            } else {
                List<Integer> l = new ArrayList<>();
                l.add(v2);
                graph.put(v1, l);
            }
            if (graph.containsKey(v2)) {
                graph.get(v2).add(v1);
            } else {
                List<Integer> l = new ArrayList<>();
                l.add(v1);
                graph.put(v2, l);
            }
        }

        List<Integer> cupidsMaxPath = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            cupidsMaxPath.add(i, 0);
        }


        updateCupidsShortestPath(n, cupidsOfNode, cupidsMaxPath, graph);


        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m; i++) {
            sb.append(cupidsMaxPath.get(i)).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);

        System.out.println(sb.toString());
    }

    private static void updateCupidsShortestPath(int n, List<Integer> cupidsOfNode, List<Integer> cupidsMaxPath, Map<Integer, List<Integer>> graph) {

        int[][] distances = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    distances[i][j] = 0;
                } else {
                    distances[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        for (Map.Entry<Integer, List<Integer>> entry : graph.entrySet()) {
            for (Integer v : entry.getValue()) {
                distances[entry.getKey() - 1][v - 1] = 1;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    if (distances[i][j] > distances[i][k] + distances[k][j]) {
                        distances[i][j] = distances[i][k] + distances[k][j];
                    }
                }
            }
        }

        System.out.println();


    }

    private static void updateCupidsShortestPath2(int s, int n, List<Integer> cupids, List<Integer> cupidsMaxPath, Map<Integer, List<Integer>> graph) {

        LinkedList<Integer> junctionsToCheck = new LinkedList<>();
        junctionsToCheck.addLast(s);

        List<Integer> distances = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            distances.add(i, -1);
        }
        distances.set(s - 1, 0);

        boolean[] isExplored = new boolean[n];

        while (!junctionsToCheck.isEmpty()) {
            int node = junctionsToCheck.removeFirst();
            if (graph.containsKey(node)) {
                for (int nextNode : graph.get(node)) {
                    if (!isExplored[nextNode - 1]) {
                        isExplored[nextNode - 1] = true;
                        junctionsToCheck.addLast(nextNode);
                        distances.set(nextNode - 1, distances.get(node - 1) + 1);
                    }
                }
            }
        }

        int cupid = cupids.get(s - 1);

        int maxDistance = 0;
        for (int i = 1; i <= n; i++) {
            if (i != s && cupids.get(i - 1) == cupid && distances.get(i - 1) > maxDistance) {
                maxDistance = distances.get(i - 1);

            }
        }

        if (maxDistance > cupidsMaxPath.get(cupid - 1)) {
            cupidsMaxPath.set(cupid - 1, maxDistance);
        }

    }


}
