package hackerearth.marchclash;

import java.util.Scanner;

/**
 * Created by akash on 26-03-2016.
 */
public class Kian {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        long series1 = 0;
        long series2 = 0;
        long series3 = 0;

        for (int i = 1; i <= n; i++) {
            int val = in.nextInt();

            if (i >= 1 && (i - 1) % 3 == 0) {
                series1 += val;
            } else if (i >= 2 && (i - 2) % 3 == 0) {
                series2 += val;
            } else if (i >= 3 && (i - 3) % 3 == 0) {
                series3 += val;
            }

        }

        System.out.println(series1 + " " + series2 + " " + series3);

    }


}
