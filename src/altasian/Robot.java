package altasian;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 26-03-2016.
 */
public class Robot {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String res;
        String _instructions;
        try {
            _instructions = in.nextLine();
        } catch (Exception e) {
            _instructions = null;
        }

        res = compute(_instructions);
        System.out.println(res);
        //bw.write(res);
        //bw.newLine();

        //bw.close();
    }

    private static String compute(String instructions) {

        int[] docks = new int[10];
        for (int i = 0; i < 10; i++) {
            docks[i] = 0;
        }

        int pos = 0;
        boolean hold = false;
        for (Character c : instructions.toCharArray()) {
            if (c == 'P') {
                pos = 0;
                hold = true;
            } else if (c == 'M' && pos < 9) {
                pos++;
            } else if (c == 'L' && hold && docks[pos] < 15) {
                docks[pos] = docks[pos] + 1;
                hold = false;
            }

        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append(Integer.toHexString(docks[i]).toUpperCase());
        }

        return sb.toString();
    }

}
