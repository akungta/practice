package altasian;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 26-03-2016.
 */
public class ConvertBase {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String res;
        long _in;
        _in = Long.parseLong(in.nextLine());

        res = convert(_in);
        System.out.println(res);
        //bw.write(res);
        //bw.newLine();

        //bw.close();
    }

    private static String convert(long in) {
        String s = Long.toString(in, 7);
        StringBuilder sb = new StringBuilder();
        for(Character c : s.toCharArray()){
            switch (c){
                case '0': sb.append("0"); break;
                case '1': sb.append("a"); break;
                case '2': sb.append("t"); break;
                case '3': sb.append("l"); break;
                case '4': sb.append("s"); break;
                case '5': sb.append("i"); break;
                case '6': sb.append("n"); break;
            }
        }
        return sb.toString();
    }

}
