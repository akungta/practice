package altasian;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 26-03-2016.
 */
public class DataValidation {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String res;
        String _input;
        try {
            _input = in.nextLine();
        } catch (Exception e) {
            _input = null;
        }

        res = validate(_input);
        //bw.write(res);
        //bw.newLine();

        //bw.close();
    }

    private static String validate(String input) {

        String[] inputElements = input.split("\\|");

        boolean row = true;
        int records = 0;
        int empty =0;
        int maxFields = 0;
        int fields = 0;
        String lastField = "";
        for (int i = 1; i < inputElements.length; i++) {
            String element = inputElements[i];
            if ("~n".equals(element) || "~~n".equals(element)) {
                if(fields > maxFields){
                    maxFields = fields;
                }
                fields = 0;
                records++;
            } else {
                fields++;
                if("".equals(element) || "~".equals(element)){
                    empty++;
                }
            }
        }

        return records + ":" + maxFields + ":" + empty + ":" + lastField;
    }


}
