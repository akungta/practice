package altasian;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by akash on 26-03-2016.
 */
public class LookAndSay {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String res;
        String _start;
        try {
            _start = in.nextLine();
        } catch (Exception e) {
            _start = null;
        }

        int _n;
        _n = Integer.parseInt(in.nextLine());

        res = LookAndSay(_start, _n);
        System.out.println(res);
        //bw.write(res);
        //bw.newLine();

        //bw.close();
    }

    static Map<String, String> cached = new HashMap<>();

    static String LookAndSay(String start, int n) {

        if (!cached.containsKey(start)) {

            StringBuilder sb = new StringBuilder();

            char[] charArr = start.toCharArray();
            int currentCharCount = 1;
            Character prevChar = charArr[0];
            for (int i = 1; i < charArr.length; i++) {
                if (charArr[i] == prevChar) {
                    currentCharCount++;
                } else {
                    sb.append(currentCharCount).append(prevChar);
                    prevChar = charArr[i];
                    currentCharCount = 1;
                }
            }
            sb.append(currentCharCount).append(prevChar);

            cached.put(start, sb.toString());
        }

        if (n == 1) {
            return cached.get(start);
        }
        return LookAndSay(cached.get(start), n - 1);
    }

}
