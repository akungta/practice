package dailyhunt;

import java.util.Scanner;

/**
 * Created by akash on 27-04-2016.
 */
public class AlphabetCount {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        int[] output = counts(s);
        StringBuilder print = new StringBuilder();
        for(int v : output){
            print.append(v).append(" ");
        }
        print.deleteCharAt(print.length()-1);
        System.out.println(print.toString());
    }

    private static int[] counts(String s) {
        int[] retval = new int[26];
        for(char c : s.toCharArray()){
            int i = c;
            if(i>=65 && i<=90){
                retval[i-65] = retval[i-65] + 1;
            } else if(i>=97 && i<=122){
                retval[i-97] = retval[i-97] + 1;
            }
        }
        return retval;
    }


}
