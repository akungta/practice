package dailyhunt;

import java.util.Scanner;

/**
 * Created by akash on 27-04-2016.
 */
public class MergeSort {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        int m = in.nextInt();
        int[] b = new int[m];
        for (int i = 0; i < m; i++) {
            b[i] = in.nextInt();
        }
        int[] merged = merge(a, b);
        StringBuilder print = new StringBuilder();
        for (int v : merged) {
            print.append(v).append(" ");
        }
        print.deleteCharAt(print.length() - 1);
        System.out.println(print.toString());
    }

    private static int[] merge(int[] a, int[] b) {
        int[] retval = new int[a.length + b.length];

        int i = 0;
        int j = 0;

        int k = 0;

        while (i < a.length && j < b.length) {
            if (a[i] < b[j]) {
                retval[k] = a[i];
                i++;
            } else {
                retval[k] = b[j];
                j++;
            }
            k++;
        }

        System.arraycopy(a, i, retval, k, a.length - i);
        System.arraycopy(b, j, retval, k, b.length - j);

        return retval;
    }


}
