package dailyhunt;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 27-04-2016.
 */
public class NumberCount {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        List<Integer> output = counts(s);
        for(int v : output){
            System.out.println(v);
        }
    }

    private static List<Integer> counts(String s) {
        List<Integer> retval = new ArrayList<>();
        boolean isPreviousCharNum = false;
        StringBuilder sb = new StringBuilder();
        for(char c : s.toCharArray()){
            if(isPreviousCharNum){
                if(isCurrentCharNum(c)){
                    sb.append(c);
                    isPreviousCharNum = true;
                } else {
                    retval.add(Integer.parseInt(sb.toString()));
                    isPreviousCharNum = false;
                }
            } else {
                if(isCurrentCharNum(c)){
                    sb = new StringBuilder();
                    sb.append(c);
                    isPreviousCharNum = true;
                }
            }
        }
        if(isPreviousCharNum){
            retval.add(Integer.parseInt(sb.toString()));
        }
        return retval;
    }

    private static boolean isCurrentCharNum(char c) {
        int i = c;
        return i>=48 && i<=57;
    }


}
