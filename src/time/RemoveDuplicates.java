package time;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashSet;

/**
 * Created by akash on 11-05-2016.
 */
public class RemoveDuplicates {

    public static void main(final String args[]) throws Exception {
        final String input = readInput();
        final String result = removeDuplicateChars(input);
        writeOutput(result);
    }

    private static String removeDuplicateChars(String input) {
        HashSet<Character> uniqueChars = new HashSet<>();
        StringBuilder retval = new StringBuilder();
        for(char c : input.toCharArray()){
            if(!uniqueChars.contains(c)){
                retval.append(c);
                uniqueChars.add(c);
            }
        }
        return retval.toString();
    }

    public static String readInput() throws Exception {
        try (final BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"))) {
            return in.readLine();
        }
    }

    public static void writeOutput(String result) throws Exception {
        new PrintStream(System.out, true, "UTF-8").println(result);
    }



}
