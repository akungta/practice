package bits;

import java.util.Scanner;

/**
 * Created by akash on 07-02-2016.
 */
public class Solution1 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        long[] inputs = new long[t];
        for (int i = 0; i < t; i++) {
            inputs[i] = in.nextLong();
        }
        bitFlip(inputs);
    }

    private static void bitFlip(long[] inputs) {
        for (long input : inputs) {
            //System.out.println(~input + 0x0000000100000000L);
            System.out.println( (long)Math.pow(2, 32)-1 - input);
        }
    }

}
