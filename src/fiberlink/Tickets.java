package fiberlink;

import java.util.*;

/**
 * Created by akash on 20-03-2016.
 */
public class Tickets {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();

        PriorityQueue<Integer> q = new PriorityQueue<>(new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });

        for (int i = 0; i < n; i++) {
            q.offer(in.nextInt());
        }

        int maxTotal = 0;

        for (int i = 0; i < m; i++) {
            Integer maxTicket = q.poll();
            maxTotal = maxTotal + maxTicket;
            q.offer(maxTicket-1);
        }

        System.out.println(maxTotal);
    }

}
