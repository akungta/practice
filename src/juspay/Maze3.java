package juspay;

import java.util.*;

/**
 * Created by akash on 20-03-2016.
 */
public class Maze3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<Integer, Integer> graph = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int to = in.nextInt();
            if (to != -1) {
                graph.put(i, to);
            }
        }
        int c1 = in.nextInt();
        int c2 = in.nextInt();

        List<Integer> p1 = getPath(graph, c1);
        List<Integer> p2 = getPath(graph, c2);

        //System.out.println(p1);
        //System.out.println(p2);

        int c = -1;
        int nearest = Integer.MAX_VALUE;
        boolean hasVal = false;
        for (int i = 0; i < p1.size(); i++) {
            for (int j = 0; j < p2.size(); j++) {
                if (p1.get(i).equals(p2.get(j))) {
                    if ((i + j) < nearest) {
                        nearest = i + j;
                        c = p1.get(i);
                    } else if ((i + j) == nearest && p1.get(i) < c) {
                        nearest = i + j;
                        c = p1.get(i);
                    }
                    break;
                }
            }
        }
        System.out.println(c);

    }

    static List<Integer> getPath(Map<Integer, Integer> graph, int c) {
        List<Integer> retval = new ArrayList<>();
        int i = c;
        retval.add(i);
        while (graph.containsKey(i)) {
            i = graph.get(i);
            if (retval.contains(i)) {
                break;
            }
            retval.add(i);
        }
        return retval;
    }


}
