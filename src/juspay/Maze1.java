package juspay;

import java.util.*;

/**
 * Created by akash on 20-03-2016.
 */
public class Maze1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<Integer, Integer> graph = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int to = in.nextInt();
            if (to != -1) {
                graph.put(i, to);
            }
        }

        boolean[] cycles = new boolean[n];

        int maxCycleSize = -1;
        for (int i = 0; i < n; i++) {
            if (!cycles[i]) {

                boolean hasCycle = false;
                int current = i;
                LinkedList<Integer> visited = new LinkedList<>();
                visited.addLast(current);

                while (!cycles[current] && graph.containsKey(current)) {
                    current = graph.get(current);

                    if (visited.contains(current)) {

                        Iterator<Integer> it = visited.iterator();
                        while (it.hasNext() && it.next() != current) {
                            it.remove();
                        }

                        for (Integer v : visited) {
                            cycles[v] = true;
                        }

                        if (maxCycleSize < visited.size()) {
                            maxCycleSize = visited.size();
                        }

                        hasCycle = true;
                        break;

                    } else {

                        visited.addLast(current);

                    }
                }

            }
        }

        System.out.println(maxCycleSize);

    }


}
