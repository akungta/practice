package juspay;

import java.util.Scanner;

/**
 * Created by akash on 20-03-2016.
 */
public class Test {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] inputs = new int[n];
        for (int i = 0; i < n; i++) {
            inputs[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 1; j <= inputs[i]; j++) {
                if (j % 3 == 0 && j % 5 == 0) {
                    System.out.println("FizzBuzz");
                } else if (j % 3 == 0) {
                    System.out.println("Fizz");
                } else if (j % 5 == 0) {
                    System.out.println("Buzz");
                } else {
                    System.out.println(j);
                }
            }
        }
    }
}
