package juspay;

import java.util.*;

/**
 * Created by akash on 20-03-2016.
 */
public class Maze2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int to = in.nextInt();
            if (to != -1) {
                if (graph.containsKey(to)) {
                    graph.get(to).add(i);
                } else {
                    List<Integer> l = new ArrayList<>();
                    l.add(i);
                    graph.put(to, l);
                }
            }
        }

        int max = 0;
        for (List<Integer> l : graph.values()) {
            if (l.size() > max) {
                max = l.size();
            }
        }

        System.out.println(max);

    }


}
