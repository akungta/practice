package accelhack;

import java.util.Scanner;

/**
 * Created by akash on 05-03-2016.
 */
public class BalancedString {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int[] input = new int[t];
        for (int i = 0; i < t; i++) {
            input[i] = in.nextInt();
        }
        for (int i = 0; i < t; i++) {
            int n = input[i];
            int factN = factorial(n);
            int p = factN;
            int v = (int) (p % (Math.pow(10, 9) + 7));
            System.out.println(v);
        }
    }

    public static int factorial(int n) {
        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

}
