package hack101;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 21-02-2016.
 */
public class BeautifulStrings {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        List<Character> input = new ArrayList<>();
        for(char c: s.toCharArray()){
            input.add(c);
        }
        System.out.println(new HashSet<>(getCombinationsList(s.length()-2, input)).size());

    }

    public static List<List<Character>> getCombinationsList(int elementCount, List<Character> inputs) {
        int N = inputs.size();

        if (elementCount < 0 || elementCount > N) {
            throw new IndexOutOfBoundsException(
                    "Can only generate permutations for a count between 0 to " + N);
        }

        List<List<Character>> retval = new ArrayList<>();

        if (elementCount == N) {
            retval.add(new ArrayList<>(inputs));
        } else if (elementCount == 1) {
            for(int i = 0 ; i < N ; i++){
                List<Character> l = new ArrayList<>();
                l.add(inputs.get(i));
                retval.add(l);
            }
        } else {
            for (int i = 0; i <= N - elementCount; i++) {
                List<Character> subList = inputs.subList(i + 1, inputs.size());

                List<List<Character>> cList = getCombinationsList(elementCount - 1, subList);
                for (List<Character> s : cList) {
                    s.add(inputs.get(i));
                    retval.add(s);
                }
            }
        }

        return retval;
    }


}
