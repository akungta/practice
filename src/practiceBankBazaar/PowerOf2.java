package practiceBankBazaar;

import java.util.Scanner;

/**
 * Created by akash on 28-02-2016.
 */
public class PowerOf2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float n = in.nextInt();

        while(n%2 == 0){
            n = n/2;
        }

        System.out.println(n == 1 ? true : false);


    }

}
