package practiceBankBazaar;

import java.util.Scanner;

/**
 * Created by akash on 27-02-2016.
 */
public class RotatedSortedArrayBS {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] input = new int[n];
        for (int i = 0; i < n; i++) {
            input[i] = in.nextInt();
        }
        int key = in.nextInt();
        binarySearch(input, key);
    }

    private static int binarySearch(int[] input, int key) {

        int low = 0;
        int high = input.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (input[mid] == key) {
                return mid;
            }

            //this array is sorted
            if (input[low] < input[mid]) {
                if (key >= input[low] && key < input[mid]) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            } else if (input[mid] < input[high]) {
                if (key > input[mid] && key <= input[high]) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
            }

        }
        return -1;

    }

}
