package practiceBankBazaar;

import java.util.Scanner;

/**
 * Created by akash on 28-02-2016.
 */
public class CountInversion {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }
        System.out.println(countInversion(arr, new int[n], 0, n - 1));

    }

    private static int countInversion(int[] arr, int[] sortedArr, int l, int r) {

        if (l == r) {
            sortedArr[l] = arr[l];
            return 0;
        }

        int retval = 0;

        int mid = (l + r) / 2;
        retval += countInversion(arr, sortedArr, l, mid);
        retval += countInversion(arr, sortedArr, mid + 1, r);

        for (int i = l ; i <= r; i++){
            arr[i] = sortedArr[i];
        }

        //sorted array index
        int k = l;
        int i = l;
        int j = mid + 1;
        while (j<=r && i <= mid) {

            if(arr[i]<=arr[j]){
                sortedArr[k] = arr[i];
                k++;
                i++;
            } else {
                sortedArr[k] = arr[j];
                k++;
                j++;
                retval += mid - i + 1;
            }

        }

        while(i <= mid){
            sortedArr[k] = arr[i];
            i++;
            k++;
        }

        while(j <= r){
            sortedArr[k] = arr[j];
            j++;
            k++;
        }

        return retval;
    }


}
