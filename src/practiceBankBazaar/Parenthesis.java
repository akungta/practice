package practiceBankBazaar;

import java.util.Scanner;

/**
 * Created by akash on 27-02-2016.
 */
public class Parenthesis {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        printParentheseis("", n, 0);
    }

    static void printParentheseis(String s, int n, int open) {
        if (n > 0) {
            printParentheseis(s + "(", n - 1, open + 1);
        }
        if (open > 0) {
            printParentheseis(s + ")", n, open - 1);
        }
        if(n == 0 && open == 0){
            System.out.println(s);
        }
    }

}
