package practiceBankBazaar;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by akash on 29-02-2016.
 */
public class TowerOfHanoi {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        LinkedList<Integer> a = new LinkedList<>();
        LinkedList<Integer> b = new LinkedList<>();
        LinkedList<Integer> c = new LinkedList<>();
        for (int i = n; i >= 1; i--) {
            a.push(i);
        }
        solveHanoi(n, "a", "b", a, b, c);

        System.out.println("Final ");
        System.out.println("A : " + a);
        System.out.println("B : " + b);
        System.out.println("C : " + c);

    }

    private static void solveHanoi(int n, String from, String to, LinkedList<Integer> a, LinkedList<Integer> b, LinkedList<Integer> c) {
        if (n == 1) {
            moveFrom(from, to, a, b, c);

            System.out.println();
            System.out.println("A : " + a);
            System.out.println("B : " + b);
            System.out.println("C : " + c);

            return;
        }
        String other = otherThan(from, to);
        solveHanoi(n - 1, from, other, a, b, c);
        moveFrom(from, to, a, b, c);
        solveHanoi(n - 1, other, to, a, b, c);
    }

    private static String otherThan(String from, String to) {
        if(("a".equals(from) && "b".equals(to)) || ("a".equals(to) && "b".equals(from))){
            return "c";
        }
        if(("a".equals(from) && "c".equals(to)) || ("a".equals(to) && "c".equals(from))){
            return "b";
        }
        if(("c".equals(from) && "b".equals(to)) || ("c".equals(to) && "b".equals(from))){
            return "a";
        }
        return null;
    }

    private static void moveFrom(String from, String to, LinkedList<Integer> a, LinkedList<Integer> b, LinkedList<Integer> c) {

        Integer val = null;
        switch (from) {
            case "a":
                val = a.isEmpty() ? null : a.pop();
                break;
            case "b":
                val = b.isEmpty() ? null : b.pop();
                break;
            case "c":
                val = c.isEmpty() ? null : c.pop();
                break;
        }

        if (val != null) {

            switch (to) {
                case "a":
                    a.push(val);
                    break;
                case "b":
                    b.push(val);
                    break;
                case "c":
                    c.push(val);
                    break;
            }

        }

    }

}
