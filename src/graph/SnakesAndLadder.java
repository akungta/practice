package graph;

import java.util.*;

/**
 * Created by akash on 13-02-2016.
 */
public class SnakesAndLadder {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int[] outputs = new int[t];
        for (int i = 0; i < t; i++) {

            Map<Integer, List<Integer>> board = new HashMap<>();
            initialize(board);

            int ladders = in.nextInt();
            Map<Integer, Integer> laddersS2E = new HashMap<>();
            for (int j = 0; j < ladders; j++) {
                int start = in.nextInt();
                int end = in.nextInt();
                laddersS2E.put(start, end);
            }

            int snakes = in.nextInt();
            Map<Integer, Integer> snakesE2S = new HashMap<>();
            for (int j = 0; j < snakes; j++) {
                int start = in.nextInt();
                int end = in.nextInt();
                snakesE2S.put(start, end);
            }

            outputs[i] = leastMovesBFS(board, laddersS2E, snakesE2S);
        }

        for (int output : outputs) {
            System.out.println(output);
        }

    }

    private static int leastMovesBFS(Map<Integer, List<Integer>> board, Map<Integer, Integer> laddersS2E, Map<Integer, Integer> snakesE2S) {
        int[] distance = new int[100];
        for (int i = 0; i < 100; i++) {
            distance[i] = -1;
        }
        distance[0] = 0;

        LinkedList<Integer> nextSquares = new LinkedList<>();
        nextSquares.addLast(1);

        while (!nextSquares.isEmpty()) {
            int square = nextSquares.removeFirst();
            if (board.containsKey(square)) {
                for (int nextSquare : board.get(square)) {

                    if (laddersS2E.containsKey(nextSquare)) {
                        nextSquare = laddersS2E.get(nextSquare);
                    }
                    if (snakesE2S.containsKey(nextSquare)) {
                        nextSquare = snakesE2S.get(nextSquare);
                    }

                    if (distance[nextSquare - 1] == -1 ) {
                        nextSquares.addLast(nextSquare);
                        distance[nextSquare - 1] = distance[square - 1] + 1;
                    }
                }
            }

        }

        return distance[99];
    }

    private static void initialize(Map<Integer, List<Integer>> board) {
        for (int i = 1; i <= 99; i++) {
            ArrayList<Integer> value = new ArrayList<>();
            for (int j = 1; j <= 6 && (i + j) <= 100; j++) {
                value.add(i + j);
            }
            board.put(i, value);
        }
    }

}
