package graph;

import java.util.*;

/**
 * Created by akash on 14-02-2016.
 */
public class BFS {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        List<List<Integer>> outputs = new ArrayList<>();
        for (int i = 0; i < t; i++) {
            Map<Integer, List<Integer>> graph = new HashMap<>();
            int nodes = in.nextInt();
            int edges = in.nextInt();
            for (int e = 0; e < edges; e++) {
                int v1 = in.nextInt();
                int v2 = in.nextInt();
                if (graph.containsKey(v1)) {
                    graph.get(v1).add(v2);
                } else {
                    List<Integer> l = new ArrayList<>();
                    l.add(v2);
                    graph.put(v1, l);
                }
                if (graph.containsKey(v2)) {
                    graph.get(v2).add(v1);
                } else {
                    List<Integer> l = new ArrayList<>();
                    l.add(v1);
                    graph.put(v2, l);
                }
            }
            int s = in.nextInt();
            outputs.add(i, getShortestPaths(nodes, edges, graph, s));
        }
        for (List<Integer> output : outputs) {
            StringBuilder sb = new StringBuilder();
            for (int i : output) {
                sb.append(i).append(" ");
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }

    }

    private static List<Integer> getShortestPaths(int nodes, int edges, Map<Integer, List<Integer>> graph, int s) {

        LinkedList<Integer> nodesToCheck = new LinkedList<>();
        nodesToCheck.addLast(s);

        int[] distances = new int[nodes];
        for (int i = 0; i < nodes; i++) {
            distances[i] = -1;
        }
        distances[s - 1] = 0;

        boolean[] isExplored = new boolean[nodes];

        while (!nodesToCheck.isEmpty()) {
            int node = nodesToCheck.removeFirst();
            if (graph.containsKey(node)) {
                for (int nextNode : graph.get(node)) {
                    if (!isExplored[nextNode - 1]) {
                        isExplored[nextNode - 1] = true;
                        nodesToCheck.addLast(nextNode);
                        distances[nextNode - 1] = distances[node - 1] + 6;
                    }
                }
            }
        }

        List<Integer> retval = new ArrayList<>();
        for (int i = 0; i < nodes; i++) {
            if (i != s - 1) {
                retval.add(distances[i]);
            }
        }
        return retval;
    }


}
