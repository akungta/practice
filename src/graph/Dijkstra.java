package graph;


import java.util.*;

/**
 * Created by akash on 15-02-2016.
 */
public class Dijkstra {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        List<List<Integer>> outputs = new ArrayList<>();
        for (int i = 0; i < t; i++) {
            int nodes = in.nextInt();
            int edges = in.nextInt();
            Map<Integer, List<GraphEdge>> graphForward = new HashMap<>();
            for (int e = 0; e < edges; e++) {
                int x = in.nextInt();
                int y = in.nextInt();
                int r = in.nextInt();
                if (graphForward.containsKey(x)) {
                    graphForward.get(x).add(new GraphEdge(x, y, r));
                } else {
                    List<GraphEdge> l = new ArrayList<>();
                    l.add(new GraphEdge(x, y, r));
                    graphForward.put(x, l);
                }
                if (graphForward.containsKey(y)) {
                    graphForward.get(y).add(new GraphEdge(y, x, r));
                } else {
                    List<GraphEdge> l = new ArrayList<>();
                    l.add(new GraphEdge(y, x, r));
                    graphForward.put(y, l);
                }
            }
            int s = in.nextInt();
            outputs.add(i, dijkstraAlgo(nodes, edges, graphForward, s));
        }
        for (List<Integer> output : outputs) {
            StringBuilder sb = new StringBuilder();
            for (int i : output) {
                sb.append(i).append(" ");
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }
    }

    private static List<Integer> dijkstraAlgo(int nodes, int edges, Map<Integer, List<GraphEdge>> graph, int s) {

        Set<Integer> traversed = new HashSet<>();
        Set<Integer> notTraversed = new HashSet<>();

        int[] distances = new int[nodes];
        for (int i = 1; i <= nodes; i++) {
            distances[i - 1] = -1;
            notTraversed.add(i);
        }
        distances[s - 1] = 0;
        traversed.add(s);
        notTraversed.remove(s);


        PriorityQueue<UntraveresedNode> heap = new PriorityQueue<>(new Comparator<UntraveresedNode>(){
            @Override
            public int compare(UntraveresedNode o1, UntraveresedNode o2) {
                return Integer.valueOf(o1.k).compareTo(o2.k);
            }
        });

        for(GraphEdge e : graph.get(s)){
            heap.add(new UntraveresedNode(e.y, e.d));
        }


        while (traversed.size() != nodes) {

            UntraveresedNode node = heap.poll();

            distances[node.y - 1] = node.k;
            traversed.add(node.y);
            notTraversed.remove(node.y);

            for(GraphEdge g : graph.get(node.y)){
                if(notTraversed.contains(g.y)){
                    
                    for(UntraveresedNode n :heap){
                        if(n.y == g.y){

                        }
                    }
                }
            }

        }


        List<Integer> retval = new ArrayList<>();
        for (
                int i = 0;
                i < nodes; i++)

        {
            if (i != s - 1) {
                retval.add(distances[i]);
            }
        }

        return retval;
    }


    private static List<Integer> dijkstraAlgo2(int nodes, int edges, Map<GraphEdge2, Integer> graph, int s) {

        Set<Integer> traversed = new HashSet<>();
        Set<Integer> notTraversed = new HashSet<>();

        int[] distances = new int[nodes];
        for (int i = 1; i <= nodes; i++) {
            distances[i - 1] = -1;
            notTraversed.add(i);
        }
        distances[s - 1] = 0;
        traversed.add(s);
        notTraversed.remove(s);

        while (traversed.size() != nodes) {

            int x = 0, y = 0, d = Integer.MAX_VALUE;

            for (int u : traversed) {
                for (int v : notTraversed) {
                    GraphEdge2 ge = new GraphEdge2(u, v);
                    if (graph.containsKey(ge)) {
                        if (distances[u - 1] + graph.get(ge) < d) {
                            d = distances[u - 1] + graph.get(ge);
                            x = u;
                            y = v;
                        }
                    }
                }
            }

            if (y != 0 && d > -1) {
                traversed.add(y);
                notTraversed.remove(y);
                distances[y - 1] = d;
            }

        }


        List<Integer> retval = new ArrayList<>();
        for (
                int i = 0;
                i < nodes; i++)

        {
            if (i != s - 1) {
                retval.add(distances[i]);
            }
        }

        return retval;
    }

    static class UntraveresedNode {
        final int y;
        final int k;

        public UntraveresedNode(int y, int d) {
            this.y = y;
            this.k = d;
        }
    }


    static class GraphEdge {

        final int x;
        final int y;
        final int d;

        public GraphEdge(int x, int y, int d) {
            this.x = x;
            this.y = y;
            this.d = d;
        }

    }

    static class GraphEdge2 {

        final int x;
        final int y;

        public GraphEdge2(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GraphEdge2 edge = (GraphEdge2) o;

            if (x != edge.x) return false;
            return y == edge.y;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }

}
