import java.util.Scanner;

/**
 * Created by akash on 16-02-2016.
 */
public class weekofcode19 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int d = in.nextInt();
        int e = in.nextInt();
        int f = in.nextInt();

        int ret = 0;

        if ((a + e + d) < ret) {
            ret = a + e + d;
        }
        if ((a + b + f) < ret) {
            ret = a + b + f;
        }
        if ((a + b + c + d) < ret) {
            ret = a + b + c + d;
        }

        System.out.println(ret == 0 ? -1 : -ret);

    }

}
