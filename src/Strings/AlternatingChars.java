package Strings;

import java.util.Scanner;

/**
 * Created by akash on 11-02-2016.
 */
public class AlternatingChars {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        String[] strings = new String[n];
        for(int i = 0; i< n ; i++){
            strings[i] = in.nextLine();
        }
        for(int res : changeStr(strings)){
            System.out.println(res);
        }
    }

    private static int[] changeStr(String[] strings) {
        int[] retval =  new int[strings.length];
        for(int i = 0; i< strings.length; i++){
            char[] chars = strings[i].toCharArray();
            char prev = chars[0];
            int deletes = 0;
            for(int j = 1 ; j < chars.length; j++){
                if( chars[j] == prev ){
                    deletes++;
                } else {
                    prev = chars[j];
                }
            }
            retval[i] = deletes;
        }
        return retval;
    }

}
