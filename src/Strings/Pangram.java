package Strings;

import java.util.Scanner;

/**
 * Created by akash on 07-02-2016.
 */
public class Pangram {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        //97 to 122
        boolean[] flagArr = new boolean[26];

        s = s.toLowerCase().replace(" ", "");
        //System.out.println(s);
        for (char c : s.toCharArray()) {
            int val = (int) c;
            if (val >= 97 && val <= 122) {
                flagArr[val - 97] = true;
            }
        }
        boolean pangram = true;
        for (boolean f : flagArr) {
            if (!f) {
                pangram = false;
                break;
            }
        }
        System.out.println(pangram ? "pangram" : "not pangram");

    }

}
