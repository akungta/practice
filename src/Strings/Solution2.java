package Strings;

import java.util.Scanner;

/**
 * Created by akash on 07-02-2016.
 */
public class Solution2 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int t = Integer.parseInt(in.nextLine());

        for (int i = 0; i < t; i++) {
            String s = in.nextLine();
            System.out.println(isFunny(s) ? "Funny" : "Not Funny");
        }

    }

    private static boolean isFunny(String s) {
        boolean ret = false;
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length() - 1; i++) {
            if (Math.abs((int) chars[i + 1] - ((int) chars[i])) ==
                    Math.abs((int) chars[s.length() - 1 - i] - ((int) chars[s.length() - i - 2]))) {
                ret = true;
            } else {
                ret = false;
                break;
            }
        }
        return ret;
    }

}
