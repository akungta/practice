package zenify;

import java.util.*;

/**
 * Created by akash on 09-02-2016.
 */
public class Solution4 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<List<Integer>> a = new ArrayList<>();
        Map<Integer, Set<Integer>> indexes = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int inputSize = in.nextInt();
            List<Integer> inputs = new ArrayList<>();
            for (int j = 0; j < inputSize; j++) {
                int next = in.nextInt();
                inputs.add(j, next);
                if (indexes.containsKey(next)) {
                    indexes.get(next).add(i);
                } else {
                    Set<Integer> l = new HashSet<>();
                    l.add(i);
                    indexes.put(next, l);
                }
            }
            a.add(i, inputs);
        }
        int[] b = new int[n];
        fillB(n, a, b, indexes);

    }

    private static void fillB(int n, List<List<Integer>> a, int[] b, Map<Integer, Set<Integer>> indexes) {

        List<Integer> currents = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (!indexes.containsKey(i)) {
                b[i] = 1;
                currents.add(i);
            }
        }

        Set<Integer> valsInCurrent = new HashSet<>();
        for (int current : currents) {
            valsInCurrent.addAll(a.get(current));
        }

        for(int val :  valsInCurrent){
           // if(indexes.get(val)){

            //}
        }


    }


}
