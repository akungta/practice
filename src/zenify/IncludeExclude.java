package zenify;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 09-02-2016.
 */
public class IncludeExclude {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;

        int _num_size = 0;
        _num_size = Integer.parseInt(in.nextLine());
        int[] _num = new int[_num_size];
        int _num_item;
        for (int _num_i = 0; _num_i < _num_size; _num_i++) {
            _num_item = Integer.parseInt(in.nextLine());
            _num[_num_i] = _num_item;
        }

        res = stealMangoes(_num);
        System.out.println(res);
        //bw.write(String.valueOf(res));
        //bw.newLine();

        //bw.close();
    }

    private static int stealMangoes(int[] num) {

        int include = num[0];
        int exclude = 0;
        int exclude2;
        int i;

        for (i = 1; i < num.length - 1; i++) {
            exclude2 = (include > exclude) ? include : exclude;

            include = exclude + num[i];
            exclude = exclude2;
        }

        return ((include > exclude) ? include : exclude);
    }

}
