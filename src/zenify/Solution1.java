package zenify;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 09-02-2016.
 */
public class Solution1 {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int[] res;

        int _words_size = 0;
        _words_size = Integer.parseInt(in.nextLine());
        String[] _words = new String[_words_size];
        String _words_item;
        for (int _words_i = 0; _words_i < _words_size; _words_i++) {
            try {
                _words_item = in.nextLine();
            } catch (Exception e) {
                _words_item = null;
            }
            _words[_words_i] = _words_item;
        }

        res = encrypt(_words);

        for (int res_i = 0; res_i < res.length; res_i++) {
            System.out.println((String.valueOf(res[res_i])));
            //bw.newLine();
        }

       // bw.close();
    }

    private static int[] encrypt(String[] words) {
        int[] retval = new int[words.length];
        for (int w = 0 ; w < words.length ; w++) {

            char[] chars = words[w].toCharArray();
            int operations = 0;
            for (int i = 0; i < chars.length / 2; i++) {
                int j = chars.length - 1 - i;

                if (chars[i] == chars[j]) {
                    //do nothing
                } else {
                    operations += Math.abs((int) chars[j] - (int) chars[i]);
                }

            }
            retval[w] = operations;
        }
        return retval;
    }
}
