package zenify;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by akash on 09-02-2016.
 */
public class Solution2 {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;

        int _prices_size = 0;
        _prices_size = Integer.parseInt(in.nextLine());
        int[] _prices = new int[_prices_size];
        int _prices_item;
        for (int _prices_i = 0; _prices_i < _prices_size; _prices_i++) {
            _prices_item = Integer.parseInt(in.nextLine());
            _prices[_prices_i] = _prices_item;
        }

        res = maxProfit(_prices);
        System.out.println(res);
        //bw.write(String.valueOf(res));
        //bw.newLine();

        //bw.close();
    }

    private static int maxProfit(int[] prices) {
        int maxProfit = -1;
        int buy = prices[0];
        int sell = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > sell) {
                sell = prices[i];
            }
            if(prices[i] < buy){
                buy = prices[i];
                sell = 0;
            }
            if ((sell - buy) > maxProfit) {
                maxProfit = sell - buy;
            }
        }
        return maxProfit < 0 ? -1 : maxProfit;
    }

    private static int maxProfit3(int[] prices) {
        int maxProfit = -1;
        for (int i = 0; i < prices.length; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                if ((prices[j] - prices[i]) > maxProfit) {
                    maxProfit = (prices[j] - prices[i]);
                }
            }
        }
        return maxProfit < 0 ? -1 : maxProfit;
    }


    private static int maxProfit2(int[] prices) {
        int maxProfit = 0;
        for (int i = 0; i < prices.length; i++) {

            int buy = prices[i];
            int sell = 0;

            for (int j = i + 1; j < prices.length; j++) {
                if (prices[j] > sell) {
                    sell = prices[j];
                }
            }

            if ((sell - buy) > maxProfit) {
                maxProfit = sell - buy;
            }

        }
        return maxProfit <= 0 ? -1 : maxProfit;
    }
}
