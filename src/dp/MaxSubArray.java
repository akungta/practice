package dp;

import java.util.Scanner;

/**
 * Created by akash on 12-02-2016.
 */
public class MaxSubArray {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int[] maxContSub = new int[t];
        int[] maxNonContSub = new int[t];
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int j = 0; j < n; j++) {
                arr[j] = in.nextInt();
            }
            maxContSub[i] = maxContSub(arr);
            maxNonContSub[i] = maxNonContSub(arr);
        }
        for (int i = 0; i < t; i++) {
            System.out.println(maxContSub[i] + " " + maxNonContSub[i]);
        }

    }

    private static int maxNonContSub(int[] arr) {
        int retval = -10000;
        for (int i = 0; i < arr.length; i++) {
            retval = Math.max(arr[i], Math.max(retval, retval + arr[i]));
        }
        return retval;
    }

    private static int maxContSub(int[] arr) {
        int retval = arr[0];
        int maxPrev = arr[0];
        for (int i = 1; i < arr.length; i++) {
            maxPrev = Math.max(arr[i], maxPrev +  arr[i]);
            retval = Math.max(retval, maxPrev);
        }
        return retval;
    }

}
