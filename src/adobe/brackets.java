package adobe;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by akash on 25-03-2016.
 */
public class brackets {

    public static void main(String[] args) {
        System.out.println(true & false);
        Scanner in = new Scanner(System.in);
        String s = in.next();
        LinkedList<Character> stack = new LinkedList<>();
        boolean isBreak = false;
        for (Character c : s.toCharArray()) {
            switch (c) {
                case '{':
                case '(':
                case '[':
                    stack.push(c);
                    break;
                case '}':
                    if (stack.peek()!= null && stack.peek() == '{') {
                        stack.pop();
                    } else {
                        isBreak = true;
                    }
                    break;
                case ']':
                    if (stack.peek() == '[') {
                        stack.pop();
                    } else {
                        isBreak = true;
                    }
                    break;
                case ')':
                    if (stack.peek() == '(') {
                        stack.pop();
                    } else {
                        isBreak = true;
                    }
                    break;

            }
            if (isBreak) {
                break;
            }
        }
        if (isBreak || !stack.isEmpty()) {
            System.out.println(0);
        } else {
            System.out.println(1);
        }


    }

}
