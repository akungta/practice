package adobe;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Created by akash on 25-03-2016.
 */
public class SumPossible {

    public static void main(String args[]) {
        int x, y;
        x = 5 >> 2;
        y = x >>> 2;
        System.out.println(y);
    }

    static int isSumPossible(Vector<Integer> a , int N) {

        Set<Integer> arSet = new HashSet<>(a);
        for(Integer i : a){
            Integer diff = N - i;
            if(arSet.contains(diff)){
                return 1;
            }
        }
        return 0;
    }

    static int isIntegerPalindrome(int a) {
        String original = String.valueOf(a);
        String reverse = "";
        for ( int i = original.length() - 1; i >= 0; i-- ) {
            reverse = reverse + original.charAt(i);
        }
        if (original.equals(reverse)) {
            return 1;
        }
        return 0;
    }

}
