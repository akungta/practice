package boomerang;

import java.util.*;

/**
 * Created by akash on 09-05-2016.
 */
public class Problem2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(in.next());
        }
        Collections.sort(list);
        HashSet<String> set = new HashSet<>();

        int ret = 0;

        if ("-".equals(list.get(0))) {
            boolean flag = true;
            for (int i = 1; i < list.size(); i++) {
                String current = list.get(i);
                if ("R".equals(current) || "L".equals(current)) {
                    set.add(current);
                } else {
                    String prev = current.substring(0, current.length() - 1);
                    if (!set.contains(prev)) {
                        ret = 0;
                        flag = false;
                        break;
                    }
                    set.add(current);
                }
            }
            if (flag) {
                ret = 1;
            }
        }

        System.out.println(ret);

    }

}
