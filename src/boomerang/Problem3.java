package boomerang;

import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by akash on 09-05-2016.
 */
public class Problem3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = Integer.parseInt(in.nextLine());
        int[] outputs = new int[t];
        for (int i = 0; i < t; i++) {
            outputs[i] = 0;
        }
        HashSet<Character> vowels = new HashSet<>();
        vowels.add('a');
        vowels.add('e');
        vowels.add('i');
        vowels.add('o');
        vowels.add('u');
        for (int i = 0; i < t; i++) {
            int count = 0;
            String input = in.nextLine().toLowerCase();
            for (char c : input.toCharArray()) {
                if (vowels.contains(c)) {
                    count++;
                }
            }
            outputs[i] = count;
        }
        for (int i = 0; i < t; i++) {
            System.out.println(outputs[i]);
        }

    }

}
