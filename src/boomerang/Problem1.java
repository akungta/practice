package boomerang;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by akash on 09-05-2016.
 */
public class Problem1 {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        in.next();
        int n = in.nextInt();
        long[] nFriends = new long[n];
        for(int i = 0; i < n ; i++){
            nFriends[i] = in.nextLong();
        }
        in.next();
        int m = in.nextInt();

        int output = 0;
        if(m > n){

            Arrays.sort(nFriends);

            for(int i=0; i<m;i++){
                long val = in.nextLong();
                if(Arrays.binarySearch(nFriends, val) != -1){
                    output++;
                }
                if(output >= n){
                    break;
                }
            }

        }  else {

            long[] friendsOfM = new long[m];
            for(int i = 0; i < m ; i++){
                friendsOfM[i] = in.nextLong();
            }
            Arrays.sort(friendsOfM);
            for(int i=0; i<n;i++){
                if(Arrays.binarySearch(friendsOfM, nFriends[i]) != -1){
                    output++;
                }
                if(output >= m){
                    break;
                }
            }
        }

        System.out.println(output);
    }

    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        in.next();
        int n = in.nextInt();
        long[] nFriends = new long[n];
        for(int i = 0; i < n ; i++){
            nFriends[i] = in.nextLong();
        }
        in.next();
        int m = in.nextInt();

        int output = 0;
        if(m > n){
            HashSet<Long> friendsOfN = new HashSet<>();
            for(int i = 0; i < n ; i++){
                friendsOfN.add(nFriends[i]);
            }
            for(int i=0; i<m;i++){
                long val = in.nextLong();
                if(friendsOfN.contains(val)){
                    output++;
                }
                if(output >= n){
                    break;
                }
            }

        }  else {

            HashSet<Long> friendsOfM = new HashSet<>();
            for(int i = 0; i < m ; i++){
                friendsOfM.add(in.nextLong());
            }
            for(int i=0; i<n;i++){
                if(friendsOfM.contains(nFriends[i])){
                    output++;
                }
                if(output >= m){
                    break;
                }
            }
        }

        System.out.println(output);
    }

}
