package intuit;

import java.io.IOException;
import java.util.*;

/**
 * Created by akash on 01-05-2016.
 */
public class MissingWords {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String[] res;
        String _s;
        try {
            _s = in.nextLine();
        } catch (Exception e) {
            _s = null;
        }

        String _t;
        try {
            _t = in.nextLine();
        } catch (Exception e) {
            _t = null;
        }

        res = missingWords(_s, _t);

        for (int res_i = 0; res_i < res.length; res_i++) {
            //bw.write(String.valueOf(res[res_i]));
            //bw.newLine();
            System.out.println(String.valueOf(res[res_i]));
        }

        //bw.close();
    }

    private static String[] missingWords(String s, String t) {
        String[] swords = s.split("\\s+");
        String[] twords = t.split("\\s+");

        List<String> retval = new ArrayList<>();

        int j = 0;
        int i = 0;
        while (i < swords.length && j < twords.length) {
            if (swords[i].equals(twords[j])) {
                j++;
            } else {
                retval.add(swords[i]);
            }
            i++;
        }

        while(i < swords.length){
            retval.add(swords[i]);
            i++;
        }

        String[] ret = new String[retval.size()];
        retval.toArray(ret);
        return ret;
    }

}
