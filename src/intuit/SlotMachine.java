package intuit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by akash on 22-03-2016.
 */
public class SlotMachine {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;

        int _result_size = Integer.parseInt(in.nextLine());
        String[] _result = new String[_result_size];
        String _result_item;
        for (int _result_i = 0; _result_i < _result_size; _result_i++) {
            try {
                _result_item = in.nextLine();
            } catch (Exception e) {
                _result_item = null;
            }
            _result[_result_i] = _result_item;
        }

        res = diceGame(_result);
        System.out.println(res);
        //bw.write(String.valueOf(res));
        //bw.newLine();

        //bw.close();
    }

    private static int diceGame(String[] result) {
        int length = result[0].length();

        List<Integer> max = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            max.add(i, 0);
        }

        for (String input : result) {
            List<Integer> slot = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                slot.add(Integer.parseInt(input.substring(i, i + 1)));
            }
            Collections.sort(slot);
            for (int i = 0; i < length; i++) {
                if (max.get(i) < slot.get(i)) {
                    max.set(i, slot.get(i));
                }
            }
        }

        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += max.get(i);
        }

        return sum;
    }


}
