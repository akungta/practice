package intuit;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by akash on 01-05-2016.
 */
public class Triplets {

    public static void main(String[] args) throws IOException {

        boolean a = true;
        boolean b = !true;
        boolean c = a | b;
        boolean d = a & b;
        boolean e = d ? b : c;
        System.out.println(d + " " + e);

        Scanner in = new Scanner(System.in);
        //final String fileName = System.getenv("OUTPUT_PATH");
        //BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        long res;
        long _t;
        _t = Long.parseLong(in.nextLine());


        int _d_size = 0;
        _d_size = Integer.parseInt(in.nextLine());
        int[] _d = new int[_d_size];
        int _d_item;
        for (int _d_i = 0; _d_i < _d_size; _d_i++) {
            _d_item = Integer.parseInt(in.nextLine());
            _d[_d_i] = _d_item;
        }
        Object o = 1;
        res = triplets(_t, _d);
        System.out.println(res);
        //bw.write(String.valueOf(res));
        //bw.newLine();

        //bw.close();
    }

    private static long triplets(long t, int[] d) {

        Arrays.sort(d);

        long retval = 0;

        for (int i = 0; i < d.length - 2; i++) {

            int j = i + 1;
            int k = d.length - 1;

            while (j < k) {

                if (d[i] + d[j] + d[k] > t) {
                    k--;
                } else {
                    retval += (k - j);
                    j++;
                }
            }
        }

        return retval;
    }


}
