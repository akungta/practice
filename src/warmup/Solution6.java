package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution6 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            int a = in.nextInt();
            int b = in.nextInt();
            System.out.println(squares(a,b));
        }
    }

    private static int squares(int a, int b) {
        double numB = Math.floor(Math.sqrt(b));
        double numA = Math.ceil(Math.sqrt(a));
        return (int) (numB - numA + 1) ;
    }


}
