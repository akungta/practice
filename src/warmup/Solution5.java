package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            int n = in.nextInt();
            System.out.println(div(n));
        }
    }

    private static int div(int n) {
        int retval = 0;
        int num = n;
        while (n > 0) {
            int i = n % 10;
            if (i != 0 && num % i == 0) {
                retval++;
            }
            n = n / 10;
        }
        return retval;
    }

}
