package warmup;

/**
 * Created by akash on 07-02-2016.
 */
public class Test {

    static Integer i;

    public static void main(String[] args) {
        long input=1;
        StringBuilder builder = new StringBuilder();
        t1(builder);
        t2(builder);
        System.out.println(builder.toString());
    }

    static void t1(StringBuilder b){
        b.append("1");
    }

    static void t2(StringBuilder b){
        b.append("2");
    }

}
