package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            int n = in.nextInt();
            System.out.println(decentNumber(n));
        }
    }

    private static String decentNumber(int n) {

        int pos = n / 3;
        for (int i = pos; i >= 0; i--) {
            int r = n - 3*i;
            if(r%5 == 0){
                StringBuilder sb = new StringBuilder();
                for(int j = 1 ;  j <= i; j++){
                    sb.append("555");
                }
                for(int j = 1 ;  j <= r/5; j++){
                    sb.append("33333");
                }
                return sb.toString();
            }
        }
        return String.valueOf(-1);
    }
}
