package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution4 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            System.out.println(height(n));
        }
    }

    private static long height(int n) {
        long retval = 1;
        for(int i = 0 ; i < n ; i++){
            if(i%2 == 0){
                retval = retval*2;
            } else {
                retval++;
            }
        }
        return retval;
    }

}
