package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] a = new int[n][n];
        for (int a_i = 0; a_i < n; a_i++) {
            for (int a_j = 0; a_j < n; a_j++) {
                a[a_i][a_j] = in.nextInt();
            }
        }
        System.out.println(abosulteDifferenceOf(a,n));
    }

    private static int abosulteDifferenceOf(int[][] a, int n) {
        int sum_d1=0;
        int sum_d2=0;
        int x = 0,y =0;
        for(int i =0 ; i< n ; i++){
            sum_d1=sum_d1+a[x][y];
            sum_d2=sum_d2+a[n-x-1][y];
            x++;y++;
        }
        return Math.abs(sum_d1-sum_d2);
    }

}
