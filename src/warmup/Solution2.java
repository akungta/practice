package warmup;

import java.util.Scanner;

/**
 * Created by akash on 06-02-2016.
 */
public class Solution2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int k = in.nextInt();
            int a[] = new int[n];
            for(int a_i=0; a_i < n; a_i++){
                a[a_i] = in.nextInt();
            }
            System.out.println(yesOrNo(n,k,a));
        }
    }

    private static String yesOrNo(int n, int k, int[] a) {
        int count = 0;
        for(int v : a){
            if(v <= 0){
                count++;
            }
        }
        return count >= k ? "NO" : "YES";
    }

}
