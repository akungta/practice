package visa;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by akash on 22-03-2016.
 */
public class substrings {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int l = in.nextInt();
        int m = in.nextInt();
        String s = in.next();

        Map<String, Integer> subStringCounts = new HashMap<>();
        for (int i = 0; i <= s.length() - k; i++) {
            for (int j = i + k; j <= i + l && j <= s.length(); j++) {
                String sub = s.substring(i, j);
                if (subStringCounts.containsKey(sub)) {
                    subStringCounts.put(sub, subStringCounts.get(sub) + 1);
                } else {
                    subStringCounts.put(sub, 1);
                }
            }
        }
        int max = 0;
        for (Integer v : subStringCounts.values()) {
            if (v > max) {
                max = v;
            }
        }
        System.out.println(max);
    }


}
