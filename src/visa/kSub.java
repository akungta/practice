package visa;

import java.util.Scanner;

/**
 * Created by akash on 21-03-2016.
 */
public class kSub {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        System.out.println(kSubS(k, arr));


    }

    private static long kSubS(int k, int[] nums) {
        long[] m = new long[k + 1];
        m[k-1]++;
        long ans = 0;
        int csum = k;
        for (int i = 0; i < nums.length; i++) {
            csum += (nums[i] % k);
            if (csum > k) {
                csum -= k;
            }
            ans += m[csum-1]++;
        }
        return ans;
    }


}
