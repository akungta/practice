package visa;

import java.util.Scanner;

/**
 * Created by akash on 22-03-2016.
 */
public class substr {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        String[] strings = new String[t];
        for(int i = 0 ; i < t; i++){
            strings[i] = in.next();
        }
        int[] ret = StringSimilarity(strings);
        for(int i = 0 ; i < t; i++) {
            System.out.println(ret[i]);
        }
    }

    static int[] StringSimilarity(String[] inputs) {
        int[] retval = new int[inputs.length];
        for(int i = 0 ; i < inputs.length; i++){
            String input = inputs[i];
            String[] suffixes = getSuffixes(input);
            int sum = 0;
            for(String suffix : suffixes){
                int j = 0;
                while(j < suffix.length()){
                    if(suffix.charAt(j) == input.charAt(j)){
                        j++;
                    } else {
                        break;
                    }
                }
                sum += j;
            }
            retval[i] = sum;
        }
        return retval;
    }

    private static String[] getSuffixes(String input) {
        String[] retval = new String[input.length()];
        for(int i = 0 ; i < input.length(); i++){
            retval[i] = input.substring(i);
        }
        return retval;
    }

}
